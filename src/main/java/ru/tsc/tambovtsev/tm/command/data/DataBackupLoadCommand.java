package ru.tsc.tambovtsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.Domain;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-load";

    @NotNull
    private final String DESCRIPTION = "Load backup from file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BACKUP));
        @NotNull final String base64Date = new String(base64Byte);
        @NotNull final byte[] bytes = new BASE64Decoder().decodeBuffer(base64Date);
        @NotNull final ByteArrayInputStream byteArrayInputStream =
                new ByteArrayInputStream(bytes);
        @NotNull final ObjectInputStream objectInputStream =
                new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        setDomain(domain);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
